import argparse
import os
import time
import xml.etree.ElementTree as ET
import matplotlib.pylab as plt
#import pprint
import jinja2

jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader('templates'))

template = jinja_env.get_template('layout.html')

# This is to get the commandline argument.
parser = argparse.ArgumentParser()
parser.add_argument('-s', '--savegame',
                    required=False,
                    default=1,
                    choices=["1","2","3"],
                    dest="savegame_num",
                    metavar="<savegame number>",
                    help="Only enter the number of the samegame you which to monitor: 1, 2 or 3." )
args = parser.parse_args()

dir_sep=os.path.sep


#  Setting up the savegame files:
savegame_dir = r"J:\My Documents\My Games\FarmingSimulator2019\savegame" + str(args.savegame_num)
onCreateObjects_file=savegame_dir + dir_sep + "onCreateObjects.xml"
careersSaveGame_file=savegame_dir + dir_sep + "careerSavegame.xml"
environment_file=savegame_dir + dir_sep + "environment.xml"
items_file=savegame_dir + dir_sep + "items.xml"

#### Setting up variables to be displayed
#### They are set up as global variables so they can be accessed in all functions

current_day=0

# Dictionaries of all factory, storage, and livestock data.
factories={}
storage={}
livestock={}

# List of all filltypes that are in storage.  This will be used to create an ordered chart of ONLY the filltypes currently in storage.
storage_fill_types=[]

# Variables for the webpage
title='Farming Simulator Tabs'

last_savegame_timestamp = None
savegame_timestamp = None


def main():
    '''Main() function controls the flow of program data.
        EXPECTS:  -nothing-
        RETURNS:  -nothing-'''
    load_data()

    # Need to prepare some variables before display:
    storage_fill_types.sort()
    jinja_var = {
        'current_day': current_day,
        'title': title,
        'storage_fill_types': storage_fill_types,
        'dict_storage': storage,
        'dict_factories': factories,
        'last_savegame_timestamp': last_savegame_timestamp
    }
    output_from_parsed_template = template.render(jinja_var)

    with open("farming_simulator_tabs.html", "w") as fh:
        fh.write(output_from_parsed_template)

def load_data():
    '''load_data() function controls loading data from various configuration files
        EXPECTS:  -nothing-
        RETURNS:  -nothing- '''
    load_data_environment()
    load_data_oncreateobjects()
    load_data_items()


def load_data_environment():
    '''load_data_environment() function is called from load_data().
        This function loads the files that hold in game data and weather data.
        Currently that file is environment.xml'''
    global current_day
    tree = ET.parse(environment_file)
    root = tree.getroot()
    day: object
    for day in root.iter('currentDay'):
        current_day = day.text

def load_data_oncreateobjects():
    '''load_data_oncreateobjects() function is called from load_data().
        This function loads the files that hold in game data and weather data.
        Currently that file is environment.xml'''
    global factories
    tree = ET.parse(onCreateObjects_file)
    root = tree.getroot()
    for factory in root.iter('productionFactory'):
        #print('Tag:', object.tag, 'Object:', object.attrib)

        # Determine if there is a custom name set up for the factory
        if 'customTitle' in factory.attrib:
            factory_name=factory.attrib['customTitle']
        else:
            factory_name=factory.attrib['indexName']
        factory_name_attrib = factory.attrib['indexName']

        # Initialize the dictionary for this factory
        factories[factory_name_attrib] = {}

        factories[factory_name_attrib]['name'] = factory_name

        # Iterate through the potential elements that we care about
        for detail in factory.iter():

            # The existence of 'inputProducts means that the factory is 'owned'
            if detail.tag == 'inputProducts':
                factories[factory_name_attrib]['owned'] = True
                factories[factory_name_attrib]['inputProduct'] = {}


            # Add the product and levels.  There is no good method to get max levels at this time.
            elif detail.tag == 'inputProduct':
                if 'name' in detail.attrib:
                    detail.attrib['name'] = detail.attrib['name'].replace('IP_', '')
                    factories[factory_name_attrib]['inputProduct'][detail.attrib['name']] = int(round(float(detail.attrib['fillLevel'])))

            elif detail.tag == 'outputProducts':
                factories[factory_name_attrib]['outputProduct'] = {}

            elif detail.tag == 'outputProduct':
                if 'name' in detail.attrib:
                    detail.attrib['name'] = detail.attrib['name'].replace('OP_', '')
                    factories[factory_name_attrib]['outputProduct'][detail.attrib['name']] = int(round(float(detail.attrib['fillLevel'])))

def load_data_items():
    '''load_data_items() function is called from load_data().
        This function loads the files that hold storage and animal husbandry data.
        Currently that file is items.xml'''
    global storage
    global storage_fill_types
    tree = ET.parse(items_file)
    root = tree.getroot()
    for item in root.iter('item'):
        # Storage data -- Silos and Stations
        if item.attrib['className'] == 'SiloPlaceable':
            silo_name=item.attrib['filename']
            silo_name=silo_name.split("/")[4]
            silo_name=silo_name.split(".")[0]
            storage[silo_name] = {}
            for detail in item.iter():
                if detail.tag == 'node':
                    storage[silo_name][detail.attrib['fillType']] = int(round(float(detail.attrib['fillLevel'])))
                    # Add the fillType to the storage_fill_types variable for later if the value is not already in the list
                    if detail.attrib['fillType'] not in storage_fill_types :
                        storage_fill_types.append(detail.attrib['fillType'])

        elif item.attrib['className'] == 'AnimalHusbandry':
            animal_name=item.attrib['filename']
            animal_name=animal_name.split("/")[3]
            animal_name=animal_name.split(".")[0]
            livestock[animal_name] = {}
            for detail in item.iter():
                if detail.tag == 'module':
                    if detail.attrib['name'] == 'foodSpillage':
                        livestock[animal_name]['cleanliness'] = detail.attrib['cleanlinessFactor']
                    elif detail.attrib['name'] == 'animals':
                        for animal_count in detail.iter():
                            if animal_count.tag == 'animal':
                                if animal_count.attrib['fillType'] in livestock[animal_name]:
                                    livestock[animal_name][animal_count.attrib['fillType']] = livestock[animal_name][animal_count.attrib['fillType']] + 1
                                else:
                                    livestock[animal_name][animal_count.attrib['fillType']] = 1
                    elif detail.attrib['name'] == 'pallets':
                        livestock[animal_name]['pallets'] = detail.attrib['palletFillDelta']
                    elif detail.attrib['name'] == 'water':
                        for water_level in detail.iter():
                            if water_level.tag == 'fillLevel':
                                livestock[animal_name]['water'] = water_level.attrib['fillLevel']
                    elif detail.attrib['name'] == 'food':
                        for food_levels in detail.iter():
                            if food_levels.tag == 'fillLevel':
                                if 'food' in livestock[animal_name]:
                                    livestock[animal_name]['food'][food_levels.attrib['fillType']] = food_levels.attrib['fillLevel']
                                else:
                                    livestock[animal_name]['food'] = {}
                                    livestock[animal_name]['food'][food_levels.attrib['fillType']] = food_levels.attrib['fillLevel']

def print_silos():
    '''This function will print the storage charts to the screen'''
    pass


if __name__== "__main__":
    while True:
        # get the current file datestamp.
        savegame_timestamp = time.ctime(os.path.getmtime(environment_file))

        # compare to the last loaded game time.
        if last_savegame_timestamp == None:
            last_savegame_timestamp = savegame_timestamp
            main()
        elif savegame_timestamp > last_savegame_timestamp:
            last_savegame_timestamp = savegame_timestamp
            main()
        else:
            time.sleep(120)

